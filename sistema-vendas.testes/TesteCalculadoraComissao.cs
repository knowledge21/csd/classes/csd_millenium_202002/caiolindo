﻿using NUnit.Framework;

namespace com.br.k21.SistemaVendas.Testes
{
    public class TestesCalculadoraComissao
    {
        [Test]
        public void Test_Venda_Ate_10_K()
        {
            int venda = 5000;
            int comisao = 250;

            decimal valor_calculado = CalculadoraComissao.Calcular(venda);

            Assert.AreEqual(comisao, valor_calculado);
        }
        [Test]
        public void Test_Venda_Mais_10_K()
        {
            int venda = 11000;
            int comisao = 660;

            decimal valor_calculado = CalculadoraComissao.Calcular(venda);

            Assert.AreEqual(comisao, valor_calculado);
        }
        [Test]
        public void Test_Venda_Igual_10_K()
        {
            int venda = 10000;
            int comisao = 500;

            decimal valor_calculado = CalculadoraComissao.Calcular(venda);

            Assert.AreEqual(comisao, valor_calculado);
        }
        [Test]
        public void Test_Venda_Decimal()
        {
            decimal venda = 55.59m;
            decimal comisao = 2.7795m;

            decimal valor_calculado = CalculadoraComissao.Calcular(venda);

            Assert.AreEqual(comisao, valor_calculado);
        }
    }
}