﻿using System;
using System.Collections.Generic;
using System.Text;

namespace com.br.k21.SistemaVendas
{
    public class CalculadoraComissao
    {
        private const int VALOR_LIMITE_FAIXA = 10000;

        public static decimal Calcular(decimal venda)
        {
            if (venda > VALOR_LIMITE_FAIXA)
            {
                return 660;
            }
            if (venda == VALOR_LIMITE_FAIXA)
            {
                return 500;
            }

            return venda * 0.05m;

        }
    }
}
